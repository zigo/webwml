# translation of distrib.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: distrib.pl\n"
"PO-Revision-Date: 2012-04-01 11:23+0100\n"
"Last-Translator: Marcin Owsiany <porridge@debian.org>\n"
"Language-Team: polski <debian-l10n-polish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Słowo kluczowe"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Wyświetl"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "ścieżki kończące się słowem kluczowym"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "pakiety zawierające pliki o takiej nazwie"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "pakiety zawierające pliki, których nazwa zawiera słowo kluczowe"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Dystrybucja"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "eksperymentalna"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "niestabilna"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testowa"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stabilna"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "dawna stabilna"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Architektura"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "dowolna"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Szukaj"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Czyść"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Szukaj w"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Tylko w nazwach pakietów"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Opisach"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Nazwach pakietów źródłowych"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Pokaż tylko dokładnie identyczne"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Sekcja"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-bit PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
#, fuzzy
#| msgid "64-bit PC (amd64)"
msgid "64-bit ARM (AArch64)"
msgstr "64-bit PC (amd64)"

#: ../../english/releases/arches.data:12
#, fuzzy
#| msgid "EABI ARM"
msgid "EABI ARM (armel)"
msgstr "EABI ARM"

#: ../../english/releases/arches.data:13
#, fuzzy
#| msgid "Hard Float ABI ARM"
msgid "Hard Float ABI ARM (armhf)"
msgstr "ABI ARM ze sprzętowym FPU"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd dla 32-bit PC (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32-bit PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD dla 32-bit PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD dla 64-bit PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
#, fuzzy
#| msgid "MIPS (little endian)"
msgid "64-bit MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr ""

#: ../../english/releases/arches.data:26
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:27
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:28
msgid "SPARC"
msgstr "SPARC"
